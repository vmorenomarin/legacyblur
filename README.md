**Legacy Blur**

A Breezemite and OneDark Breeze fork for blurred theme lovers.

![Legacy Blur](https://i.imgur.com/oz2k7zR.jpg) 

Sources: 

[Breezemite](https://github.com/andreyorst/Breezemite) by [andreyorst](https://www.opendesktop.org/member/458963/) and [OneDark Breeze](https://store.kde.org/p/1261434/) by [GAUGE](https://www.opendesktop.org/member/503320/).

Any comment to: vmorenomarin@gmail.com
